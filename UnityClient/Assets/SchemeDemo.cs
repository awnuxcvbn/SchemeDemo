﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_IOS
using System.Runtime.InteropServices;
#endif

public class SchemeDemo : MonoBehaviour
{
    public Text text;

    // Use this for initialization
    void Start()
    {
        GetInfo();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnLaunchInfo(string launchInfo)
    {
        Debug.LogError("launchInfo:" + launchInfo);
        text.text = launchInfo;
    }

    public void GetInfo()
    {
        
#if UNITY_ANDROID
        AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject>("currentActivity");
        jo.Call("getLaunchInfo");
#elif UNITY_IOS
        _GetLaunchInfo();
#endif
    }

    private void OnApplicationFocus(bool focus)
    {
        Debug.LogError("focus:" + focus);
        if (true)
        {
            GetInfo();
        }
    }

#if UNITY_IOS
    [DllImport("__Internal")]
    private static extern void _GetLaunchInfo();
#endif
}